## Git bash
это приложение для сред Microsoft Windows, которое предоставляет эмуляцию bash, используемую для запуска Git из командной строки. Это не простой bash, скомпилированный для Windows, а пакет, содержащий Bash, SSH, SCP и некоторые другие утилиты Unix, скомпилированные для Windows. Он также содержит новое окно терминала интерфейса командной строки под названием minty. Эти утилиты связаны с этим пакетом Bash, чтобы создать полезный пакет программного обеспечения.
## Что делает команда git pull? Чем она отличается от git push?
**_git pull_**
используется для извлечения и загрузки содержимого из удаленного репозитория и немедленного обновления локального репозитория этим содержимым. Команда git pull работает как комбинация команд git fetch и git merge, т.е. Git вначале забирает изменения из указанного удалённого репозитория, а затем пытается слить их с текущей веткой.
**_git push_**
используется для выгрузки содержимого локального репозитория в удаленный репозиторий. Она позволяет передать коммиты из локального репозитория в удаленный.
## Что такое merge request?
запрос на слияние веток. Необходимость использования этой функции может возникнуть тогда, когда нужно перенести функциональность из одной ветки в другую.
## Чем между собой отличаются команды git status и git log?
Команда _git status_ отображает состояние рабочего каталога и раздела проиндексированных файлов. С ее помощью можно проверить индексацию изменений и увидеть файлы, которые не отслеживаются Git. Информация об истории коммитов проекта не отображается при выводе данных о состоянии. Для этого используется команда _git log_ . Она нужна для просмотра истории коммитов, начиная с самого свежего и уходя к истокам проекта. По умолчанию, она показывает лишь историю текущей ветки, но может быть настроена на вывод истории других, даже нескольких сразу, веток.
## Что такое submodule (подмодуль)? С помощью какой команды можно добавлять сабмодули в свой репозиторий?
это просто стандартный репозиторий Git.Единственное, что делает репозиторий Git подмодулем, — это то, что он помещается в другой, родительский репозиторий Git.Он остается полностью функциональным репозиторием: в нем можно выполнять все действия, которые вы уже знаете из своей «нормальной» работы с Git — от изменения файлов до фиксации, извлечения и отправки. В подмодуле возможно все.
Для добавления нового подмодуля необходимо использовать команду _git submodule add_, указав относительный или абсолютный URL проекта, который вы хотите начать отслеживать.
